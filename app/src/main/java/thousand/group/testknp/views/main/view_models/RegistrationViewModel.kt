package thousand.group.testknp.views.main.view_models

import com.google.gson.JsonObject
import thousand.group.testknp.R
import thousand.group.testknp.global.base.BaseViewModel
import thousand.group.testknp.global.helpers.ResErrorHelper
import thousand.group.testknp.global.extension.isEmailValid
import thousand.group.testknp.global.simple.AppConstants
import thousand.group.testknp.global.system.ResourceManager
import thousand.group.testknp.global.system.SingleLiveEvent
import thousand.group.testknp.views.main.repositories.register.RegistrationRepository

class RegistrationViewModel(
    val repository: RegistrationRepository,
    override val resourceManager: ResourceManager,
    override val errorHelper: ResErrorHelper
) : BaseViewModel(
    resourceManager,
    errorHelper
) {

    val toastLV = SingleLiveEvent<String>()

    fun regBtnClicked(
        login_edit: String,
        name_edit: String,
        surname_edit: String,
        email_edit: String,
        phone_edit: String,
        password_edit: String,
        status_edit: String
    ) {

        when {
            !email_edit.isEmailValid() -> {
                toastLV.postValue(resourceManager.getString(R.string.error_email))
                return
            }
            phone_edit.trim().length != 11 -> {
                toastLV.postValue(resourceManager.getString(R.string.error_phone))
                return
            }
        }

        val jsonObject = JsonObject()

        jsonObject.addProperty(AppConstants.ApiFieldConstants.id, 0)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.username, login_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.firstName, name_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.lastName, surname_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.email, email_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.password, password_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.phone, phone_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.userStatus, status_edit)

        repository
            .register(jsonObject)
            .subscribe({
                when (it.code()) {
                    AppConstants.StatusCodeConstants.STATUS_CODE_200 -> {
                        toastLV.postValue("Все норм")
                    }
                    else -> {
                        toastLV.postValue(errorHelper.showErrorMessageString(vmTag, it))
                    }
                }
            }, {
                it.printStackTrace()
                toastLV.postValue(errorHelper.showThrowableMessageStr(resourceManager, vmTag, it))
            })
            .connect()

    }

}