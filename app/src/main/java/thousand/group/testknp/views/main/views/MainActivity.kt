package thousand.group.testknp.views.main.views

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import thousand.group.testknp.R
import thousand.group.testknp.global.base.BaseActivity

class MainActivity : BaseActivity() {
    override var layoutResId = R.layout.activity_main


    override fun initIntent(intent: Intent?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initController() {
        reg_btn.setOnClickListener {
            val regFragment = RegistrationFragment.newInstance()

            regFragment.show(supportFragmentManager, "RegTag")
        }

        info_btn.setOnClickListener {
            val userFragment = UserInfoFragment.newInstance()

            userFragment.show(supportFragmentManager, "UserInfo")
        }
    }
}