package thousand.group.testknp.views.main.views

import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_bottom_registration.*
import thousand.group.testknp.R
import thousand.group.testknp.global.base.BaseBottomSheetDialog
import thousand.group.testknp.global.extension.observeLiveData
import thousand.group.testknp.views.main.view_models.RegistrationViewModel

class RegistrationFragment :
    BaseBottomSheetDialog<RegistrationViewModel>(RegistrationViewModel::class) {
    override var layoutResId = R.layout.fragment_bottom_registration

    companion object {

        fun newInstance(): RegistrationFragment {
            val fragment = RegistrationFragment()
            val args = Bundle()

            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.toastLV) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
    }

    override fun initController() {
        reg_up_btn.setOnClickListener {
            viewModel.regBtnClicked(
                login_edit.text.toString(),
                name_edit.text.toString(),
                surname_edit.text.toString(),
                email_edit.text.toString(),
                phone_edit.text.toString(),
                password_edit.text.toString(),
                status_edit.text.toString()
            )
        }
    }

}