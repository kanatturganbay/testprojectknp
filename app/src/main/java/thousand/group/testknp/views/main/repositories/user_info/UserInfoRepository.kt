package thousand.group.testknp.views.main.repositories.user_info

import io.reactivex.Completable
import io.reactivex.Single
import retrofit2.Response
import thousand.group.testknp.model.UserInfoModel

interface UserInfoRepository {
    fun getUser(login: String): Single<Response<UserInfoModel>>

    fun addUserInfo(userInfoModel: UserInfoModel): Completable

    fun getUserInfo(id: Long): Single<MutableList<UserInfoModel>>
}