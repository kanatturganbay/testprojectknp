package thousand.group.testknp.views.main.repositories.register

import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Response

interface RegistrationRepository {
    fun register(userData: JsonObject): Single<Response<JsonObject>>
}