package thousand.group.testknp.views.main.repositories.register

import com.google.gson.JsonObject
import thousand.group.data_base_util.global.db.data_base_util.DataBaseUtil
import thousand.group.testknp.global.services.api_services.ServerService
import thousand.group.testknp.global.system.SchedulersProvider


class RegistrationRepositoryImpl(
    private val service: ServerService,
    private val dataBaseUtil: DataBaseUtil,
    private val schedulersProvider: SchedulersProvider
) : RegistrationRepository {

    override fun register(userData: JsonObject) = service
        .register(userData)
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())

}