package thousand.group.testknp.views.main.view_models

import thousand.group.testknp.R
import thousand.group.testknp.global.base.BaseViewModel
import thousand.group.testknp.global.helpers.ResErrorHelper
import thousand.group.testknp.global.simple.AppConstants
import thousand.group.testknp.global.system.ResourceManager
import thousand.group.testknp.global.system.SingleLiveEvent
import thousand.group.testknp.model.UserInfoModel
import thousand.group.testknp.views.main.repositories.user_info.UserInfoRepository

class UserInfoViewModel(
    val repository: UserInfoRepository,
    override val resourceManager: ResourceManager,
    override val errorHelper: ResErrorHelper
) : BaseViewModel(
    resourceManager,
    errorHelper
) {

    val toastLV = SingleLiveEvent<String>()
    val setTextLV = SingleLiveEvent<String>()

    fun getUserInfo(
        login_edit: String
    ) {

        when {
            login_edit.trim().isEmpty() -> {
                toastLV.postValue(resourceManager.getString(R.string.error_login))
                return
            }
        }

        repository
            .getUser(login_edit)
            .subscribe({
                when (it.code()) {
                    AppConstants.StatusCodeConstants.STATUS_CODE_200 -> {
                        it.body()?.apply {
                            addUserDatabase(this)
                        }
                    }
                    else -> {
                        toastLV.postValue(errorHelper.showErrorMessageString(vmTag, it))
                    }
                }
            }, {
                it.printStackTrace()
                toastLV.postValue(errorHelper.showThrowableMessageStr(resourceManager, vmTag, it))
            })
            .connect()

    }

    private fun addUserDatabase(user: UserInfoModel) {
        repository
            .addUserInfo(user)
            .subscribe({
                getUserAndSetText(user.id)
            }, {
                it.printStackTrace()
                toastLV.postValue(errorHelper.showThrowableMessageStr(resourceManager, vmTag, it))
            })
            .connect()
    }

    private fun getUserAndSetText(user_id: Long) {
        repository
            .getUserInfo(user_id)
            .subscribe({
                setTextLV.postValue(it.toString())
            }, {
                it.printStackTrace()
                toastLV.postValue(errorHelper.showThrowableMessageStr(resourceManager, vmTag, it))
            })
            .connect()
    }

}