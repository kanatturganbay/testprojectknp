package thousand.group.testknp.views.main.repositories.user_info

import thousand.group.data_base_util.global.db.data_base_util.DataBaseUtil
import thousand.group.testknp.global.services.api_services.ServerService
import thousand.group.testknp.global.system.SchedulersProvider
import thousand.group.testknp.model.UserInfoModel

class UserInfoRepositoryImpl(
    private val service: ServerService,
    private val dataBaseUtil: DataBaseUtil,
    private val schedulersProvider: SchedulersProvider
) : UserInfoRepository {

    override fun getUser(login: String) = service
        .getUser(login)
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())

    override fun addUserInfo(userInfoModel: UserInfoModel) = dataBaseUtil.getDataDao()
        .addUserInfo(userInfoModel)
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())

    override fun getUserInfo(id: Long) = dataBaseUtil.getDataDao()
        .getUserInfo(id)
        .subscribeOn(schedulersProvider.io())
        .observeOn(schedulersProvider.ui())

}