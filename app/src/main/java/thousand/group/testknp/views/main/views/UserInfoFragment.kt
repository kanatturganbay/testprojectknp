package thousand.group.testknp.views.main.views

import android.os.Bundle
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_user_info.*
import thousand.group.testknp.R
import thousand.group.testknp.global.base.BaseBottomSheetDialog
import thousand.group.testknp.global.extension.observeLiveData
import thousand.group.testknp.views.main.view_models.UserInfoViewModel

class UserInfoFragment :
    BaseBottomSheetDialog<UserInfoViewModel>(UserInfoViewModel::class) {
    override var layoutResId = R.layout.fragment_user_info

    companion object {

        fun newInstance(): UserInfoFragment {
            val fragment = UserInfoFragment()
            val args = Bundle()

            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.toastLV) {
            Toast.makeText(requireContext(), it, Toast.LENGTH_SHORT).show()
        }
        observeLiveData(viewModel.setTextLV) {
            user_info_text.setText(it)
        }
    }

    override fun initController() {
        get_user_info_btn.setOnClickListener {
            viewModel.getUserInfo(
                login_edit.text.toString()
            )
        }
    }

}