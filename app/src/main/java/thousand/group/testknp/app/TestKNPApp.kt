package thousand.group.testknp.app

import android.app.Application
import io.reactivex.plugins.RxJavaPlugins
import org.koin.android.ext.koin.androidContext
import org.koin.core.context.startKoin
import thousand.group.testknp.global.di.appModule

class TestKNPApp : Application() {
    override fun onCreate() {
        super.onCreate()

        startKoin {
            androidContext(this@TestKNPApp)
            modules(appModule)
        }

        RxJavaPlugins.setErrorHandler { }
    }

}