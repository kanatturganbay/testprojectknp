package thousand.group.testknp.global.db.daos

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import io.reactivex.Completable
import io.reactivex.Single
import thousand.group.testknp.model.UserInfoModel


@Dao
interface DataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    fun addUserInfo(userInfoModel: UserInfoModel): Completable

    @Query("select * from user_info where id=:id")
    fun getUserInfo(id: Long): Single<MutableList<UserInfoModel>>

}