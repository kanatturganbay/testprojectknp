package thousand.group.testknp.global.helpers

import android.util.Log
import retrofit2.HttpException
import retrofit2.Response
import thousand.group.testknp.R
import thousand.group.testknp.global.system.ResourceManager

class ResErrorHelper {
    internal fun showThrowableMessageStr(
        resourceManager: ResourceManager,
        tag: String,
        throwable: Throwable
    ): String = when (throwable) {
        is HttpException -> {
            val error = throwable.response()?.errorBody()?.string().toString()

            Log.i(tag, error)
            error
        }
        else -> {
            Log.i(tag, throwable.localizedMessage.toString())
            resourceManager.getString(R.string.error_internet_connection)
        }
    }

    internal fun <T> showErrorMessageString(tag: String, response: Response<T>): String {
        val error = response.errorBody()?.string().toString()

        Log.i(tag, error)

        return error
    }

}