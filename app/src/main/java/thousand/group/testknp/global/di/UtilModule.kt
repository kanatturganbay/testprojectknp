package thousand.group.testknp.global.di

import android.content.Context
import org.koin.dsl.module
import thousand.group.testknp.global.helpers.ResErrorHelper
import thousand.group.testknp.global.system.AppSchedulers
import thousand.group.testknp.global.system.ResourceManager
import thousand.group.testknp.global.system.SchedulersProvider

val utilModule = module {
    single<SchedulersProvider> { AppSchedulers() }
    single { ResErrorHelper() }
    factory { (context: Context) -> ResourceManager(context) }

}