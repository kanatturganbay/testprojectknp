package thousand.group.testknp.global.di

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import thousand.group.data_base_util.global.db.data_base_util.DataBaseUtil

val dataBaseModule = module {
    single { DataBaseUtil.create(androidContext()) }
}