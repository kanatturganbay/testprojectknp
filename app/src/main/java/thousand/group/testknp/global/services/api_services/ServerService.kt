package thousand.group.testknp.global.services.api_services

import com.google.gson.JsonObject
import io.reactivex.Single
import retrofit2.Response
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import thousand.group.alcovita.global.constants.simple.Endpoints
import thousand.group.testknp.model.UserInfoModel


interface ServerService {

    @POST(Endpoints.register)
    fun register(@Body userData: JsonObject): Single<Response<JsonObject>>

    @GET(Endpoints.getUser)
    fun getUser(@Path("login") login: String): Single<Response<UserInfoModel>>

}