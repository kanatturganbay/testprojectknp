package thousand.group.testknp.global.simple

object AppConstants {
    object StatusCodeConstants {
        const val STATUS_CODE_200 = 200
    }

    object ApiFieldConstants {
        const val id = "id"
        const val username = "username"
        const val firstName = "firstName"
        const val lastName = "lastName"
        const val email = "email"
        const val password = "password"
        const val phone = "phone"
        const val userStatus = "userStatus"
    }
}