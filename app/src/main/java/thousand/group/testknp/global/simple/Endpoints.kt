package thousand.group.alcovita.global.constants.simple

object Endpoints {
    const val register = "user"
    const val getUser = "user/{login}"
}