package thousand.group.testknp.global.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.testknp.global.base.BaseViewModel
import kotlin.reflect.KClass

abstract class BaseFragment<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    Fragment() {

    var fragmentTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(requireActivity(), arguments)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(layoutResId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(savedInstanceState)
        initLiveData()
        initController()

    }

    override fun onDestroy() {
        super.onDestroy()

        viewModelStore.clear()
    }

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

}