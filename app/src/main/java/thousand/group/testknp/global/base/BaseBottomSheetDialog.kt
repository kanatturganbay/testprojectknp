package thousand.group.testknp.global.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import com.google.android.material.bottomsheet.BottomSheetDialogFragment
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.reflect.KClass

abstract class BaseBottomSheetDialog<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    BottomSheetDialogFragment() {

    var fragmentTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(requireActivity(), arguments)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(layoutResId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        initView(savedInstanceState)
        initLiveData()
        initController()

    }

    override fun onDestroy() {
        super.onDestroy()

        viewModelStore.clear()
    }

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

}