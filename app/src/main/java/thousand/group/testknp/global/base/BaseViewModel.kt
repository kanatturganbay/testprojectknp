package thousand.group.testknp.global.base

import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import thousand.group.testknp.global.helpers.ResErrorHelper
import thousand.group.testknp.global.system.ResourceManager

abstract class BaseViewModel(
    open val resourceManager: ResourceManager,
    open val errorHelper: ResErrorHelper
) : ViewModel(), LifecycleObserver {

    var vmTag: String = this.javaClass.simpleName

    private val compositeDisposable = CompositeDisposable()

    protected fun Disposable.connect() {
        compositeDisposable.add(this)
    }


}

