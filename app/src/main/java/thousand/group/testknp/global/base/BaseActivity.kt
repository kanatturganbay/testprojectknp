package thousand.group.testknp.global.base

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.inputmethod.InputMethodManager
import androidx.appcompat.app.AppCompatActivity
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import kotlin.reflect.KClass

abstract class BaseActivity:
    AppCompatActivity() {

    var activityTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int

    private var startFlag = false

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            if (startFlag) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            } else {
                startFlag = true
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)

        initIntent(intent)
        initView(savedInstanceState)
        initController()

        startFlag = true

    }

    override fun onDestroy() {
        viewModelStore.clear()
        super.onDestroy()
    }

    abstract fun initIntent(intent: Intent?)

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initController()
}