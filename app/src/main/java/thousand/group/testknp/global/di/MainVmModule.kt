package thousand.group.testknp.global.di

import android.content.Context
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import thousand.group.testknp.views.main.view_models.RegistrationViewModel
import thousand.group.testknp.views.main.view_models.UserInfoViewModel

val mainVmModule = module {
    viewModel { (context: Context) ->
        RegistrationViewModel(get(), get { parametersOf(context) }, get())
    }

    viewModel { (context: Context) ->
        UserInfoViewModel(get(), get { parametersOf(context) }, get())
    }
}