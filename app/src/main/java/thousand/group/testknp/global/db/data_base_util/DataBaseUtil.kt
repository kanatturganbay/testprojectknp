package thousand.group.data_base_util.global.db.data_base_util

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import thousand.group.testknp.global.db.daos.DataDao
import thousand.group.testknp.model.UserInfoModel

@Database(
    entities = [
        UserInfoModel::class
    ],
    version = 1,
    exportSchema = false
)
abstract class DataBaseUtil : RoomDatabase() {
    abstract fun getDataDao(): DataDao

    companion object {

        private const val TAG = "DataBaseUtil"

        fun create(context: Context): DataBaseUtil =
            Room.databaseBuilder(
                context,
                DataBaseUtil::class.java,
                DataBaseProperties.DB_NAME
            )
                .build()


    }

    object DataBaseProperties {
        val DB_NAME = "testknp.db"
    }

}