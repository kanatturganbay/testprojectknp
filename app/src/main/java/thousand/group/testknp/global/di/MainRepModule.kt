package thousand.group.testknp.global.di

import org.koin.dsl.module
import thousand.group.testknp.views.main.repositories.register.RegistrationRepository
import thousand.group.testknp.views.main.repositories.register.RegistrationRepositoryImpl
import thousand.group.testknp.views.main.repositories.user_info.UserInfoRepository
import thousand.group.testknp.views.main.repositories.user_info.UserInfoRepositoryImpl

val mainRepModule = module {
    single<RegistrationRepository> { RegistrationRepositoryImpl(get(), get(), get()) }
    single<UserInfoRepository> { UserInfoRepositoryImpl(get(), get(), get()) }
}