package thousand.group.testknp.global.di

val appModule = listOf(
    networkModule,
    dataBaseModule,
    utilModule,
    mainRepModule,
    mainVmModule
)